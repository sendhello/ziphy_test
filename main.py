from typing import Optional
from collections import deque

SOURCE = [
    (None, 'a'),
    (None, 'b'),
    (None, 'c'),
    ('a', 'a1'),
    ('a', 'a2'),
    ('a2', 'a21'),
    ('a2', 'a22'),
    ('b', 'b1'),
    ('b1', 'b11'),
    ('b11', 'b111'),
    ('b', 'b2'),
    ('c', 'c1'),
]

EXPECTED = {
    'a': {'a1': {}, 'a2': {'a21': {}, 'a22': {}}},
    'b': {'b1': {'b11': {'b111': {}}}, 'b2': {}},
    'c': {'c1': {}},
}


class Tree(dict):
    def set_node(self, record: tuple[Optional[str], str]):
        queue = deque()
        node = self

        if record[0] is None:
            node[record[1]] = {}
            return None

        while True:
            if queue:
                child_name, child_value = queue.popleft()
                node = child_value

                if record[0] == child_name:
                    child_value[record[1]] = {}
                    return None

            for child_name, child_value in node.items():
                queue.append((child_name, child_value))

            if not queue:
                raise IOError(f"Node `{record[0]}` not found")


def to_tree(source: list[tuple[Optional[str], str]]) -> dict[str, dict]:
    tree = Tree()
    for record in source:
        tree.set_node(record)

    return tree


if __name__ == '__main__':
    assert to_tree(SOURCE) == EXPECTED
